# Project 5: Brevet time calculator with Ajax and MongoDB

This calculator calculates ACP controle times, using the algorithm described here (https://rusa.org/pages/acp-brevet-control-times-calculator) and implemented here (https://rusa.org/octime_acp.html), using AJAX and Flask. 

It has buttons for submitting and displaying the control times, which is stored using MongoDB. After populating the rows, press the submit button to submit the data. Each submit clears the previous items in the database. When you are ready to see your control times, press display.

My implementation allows the user to spread their data out, with empty rows between. It will not insert the empty rows into the database, but will only insert the rows with data in them, without throwing an error.

## Test cases

### 1: No input

Without populating any of the fields, press the submit button. It should load the page /no_data, with an appropriate message.

### 2: Invalid input

Type in an invalid number into one of the fields: for instance, -10 km. Press tab or click on another field to auto-populate the open and close times. Press the submit button. It should load the page /forbidden_distances, with an approriate message.

## Contact info

This project is for CIS 322 at University of Oregon.

Arden Butterfield, abutter2@uoregon.edu