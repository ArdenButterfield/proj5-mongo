"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import os
import flask
from flask import request, redirect, url_for
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
from pymongo import MongoClient

import logging
logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY


client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')

@app.route("/forbidden_distances")
def forbidden_distances():
    app.logger.debug("Forbidden distance warning")
    return flask.render_template('forbidden_distances.html')

@app.route("/no_data")
def no_data():
    app.logger.debug("no data warning")
    return flask.render_template('no_data.html')


@app.route("/display")
def display():
    _items = db.tododb.find()
    items = [f"{item['miles']:6}{item['km']:6}{item['location']:10}{item['open']:14}{item['close']}" for item in _items]
    app.logger.debug(items)
    return flask.render_template('display.html', items=items)

@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404

def is_allowed(km, brevet_dist_km):
    """Is a control point at km on a brevet of length brevet_dist_km allowed?
     Returns boolean"""
    if km < 0:
        return False
    if km > brevet_dist_km * 1.2:
        return False
    return True

###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############

@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request in _calc_times")
    km = request.args.get('km', 999, type=float)
    brevet_dist_km = request.args.get('brevet_dist_km', 999, type=int)
    begin_date = request.args.get('begin_date', 999, type=str)
    begin_time = request.args.get('begin_time', 999, type=str)
    begin_datetime = begin_date + 'T' + begin_time

    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    open_time = acp_times.open_time(km, brevet_dist_km, begin_datetime)
    close_time = acp_times.close_time(km, brevet_dist_km, begin_datetime)
    allowed = is_allowed(km, brevet_dist_km)
    result = {"open": open_time, "close": close_time, "allowed": allowed}
    app.logger.debug(f"sending {result}")
    return flask.jsonify(result=result)

@app.route('/_submit', methods=['POST'])
def new():
    # Erase database.
    db.tododb.remove( { } )
    
    # add the stuff to the database.
    form = request.form

    miles =    form.getlist('miles')
    km =       form.getlist('km')
    location = form.getlist('location')
    open =     form.getlist('open')
    close =    form.getlist('close')
    notes =    form.getlist('notes')

    form_len = len(miles)

    if max(km) == "":
        return redirect(url_for("no_data"))
    if max(notes) != "":
        return redirect(url_for("forbidden_distances"))

    for i in range(form_len):
        if open[i] != "": # If this row actually has data
            item_doc = {
                'miles': miles[i],
                'km': km[i],
                'location': location[i],
                'open': open[i],
                'close': close[i]
                # We don't include notes, since the only notes
                # are if there is a forbidden distance, which we
                # already checked for.
            }
            app.logger.debug(f"Inserting {item_doc}")
            db.tododb.insert_one(item_doc)

    return redirect(url_for("index"))

#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
