"""
Nose tests for acp_times.py
"""

from acp_times import *

import nose

def test_provided_example1_opens():
    start = arrow.utcnow()
    assert open_time(60, 200, start.isoformat()) == start.shift(hours=1, minutes=46).isoformat()
    assert open_time(120, 200, start.isoformat()) == start.shift(hours=3, minutes=32).isoformat()
    assert open_time(175, 200, start.isoformat()) == start.shift(hours=5, minutes=9).isoformat()
    assert open_time(200, 200, start.isoformat()) == start.shift(hours=5, minutes=53).isoformat()

def test_provided_example1_closes():
    start = arrow.utcnow()
    assert close_time(60, 200, start.isoformat()) == start.shift(hours=4, minutes=0).isoformat()
    assert close_time(120, 200, start.isoformat()) == start.shift(hours=8, minutes=0).isoformat()
    assert close_time(175, 200, start.isoformat()) == start.shift(hours=11, minutes=40).isoformat()

def test_provided_example2_opens():
    start = arrow.utcnow()
    assert open_time(100, 600, start.isoformat()) == start.shift(hours=2, minutes=56).isoformat()
    assert open_time(200, 600, start.isoformat()) == start.shift(hours=5, minutes=53).isoformat()
    assert open_time(350, 600, start.isoformat()) == start.shift(hours=10, minutes=34).isoformat()
    assert open_time(550, 600, start.isoformat()) == start.shift(hours=17, minutes=8).isoformat()

def test_provided_example2_closes():
    start = arrow.utcnow()
    assert close_time(550, 600, start.isoformat()) == start.shift(hours=36, minutes=40).isoformat()
    assert close_time(600, 600, start.isoformat()) == start.shift(hours=40, minutes=0).isoformat()

def test_provided_example3():
    start = arrow.utcnow()
    assert open_time(890, 1000, start.isoformat()) == start.shift(hours=29, minutes=9).isoformat()
    assert close_time(890, 1000, start.isoformat()) == start.shift(hours=65, minutes=23).isoformat()

def test_control_at_start():
    start = arrow.utcnow()
    assert close_time(0, 900, start.isoformat()) == start.shift(hours=1).isoformat()

def test_French_algorithm():
    start = arrow.utcnow()
    assert close_time(50, 400, start.isoformat()) == start.shift(hours=3, minutes=30).isoformat()
